## Bug Overview
Provide a concise description of the bug.

## Steps to Reproduce
1. Open the terminal.
2. Run the command `[insert the specific command here]`.
3. Observe the unexpected behavior or error.

## Expected Behavior
Describe what should have happened when you followed the steps above.

## Error Message
Copy and paste any error messages that appear in the terminal output.

## Environment
- OS: [e.g., Windows11, macOS, Linux]
- rustc version: [e.g., 1.55.0]
- CLI Tool version: [e.g., 0.7.5]