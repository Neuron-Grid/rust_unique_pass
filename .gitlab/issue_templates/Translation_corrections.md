## Translation Issue Overview
Briefly describe the nature of the translation issue.

## File and Line Reference
- File: [e.g., `eng.ftl`]
- Line number(s): [e.g., 30]

## Original Text
Provide the exact text of the original English text.

## Proposed Correction
Suggest a more accurate or appropriate translation.

## Reason for Correction
Explain why the current translation is incorrect or could be improved. Mention any relevant linguistic rules, cultural context, or terminology consistency that support your correction.

## Additional Notes
Include any other comments or suggestions regarding this translation issue.