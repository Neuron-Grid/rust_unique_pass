## CONTRIBUTING
As this project is developed by me personally, I have limited resources and time.
Basically, the focus is on fixing bugs and enhancing stability.

## Future plans
- Improve the quality of the code
- Improve translations and add more supported languages
- Improve documentation

## Creating an issue
- Bug reports and translation corrections should be made according to the template.
- Suggestions for improvements to the template are also welcome.

## Additional languages supported
The project aims to be multilingual and welcomes the addition of new languages. If you would like to add a new language to the project, please follow the process below.
1. **Submit a language proposal**: propose the addition of a new language via an issue on GitLab.
2. **Start the translation process**: once your proposal has been approved, start the translation. Submit the translation files (ftl files) via a GitLab merge request.
3. **Review and feedback**: As there are currently no reviewers, translation reviews will be done by me, the project manager. I may use DeepL Translate or other translation tools as an aid to check the accuracy of the translation.
4. **Merge**: If there are no problems after review, the translation will be merged into the main branch and released in the next release.