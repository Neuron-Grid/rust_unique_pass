# CONTRIBUTING
このプロジェクトは私個人で開発しているため、リソースと時間が限られています。
基本的にはバグ修正と安定性の強化に注力します。

## 今後の予定
- コードの品質を改善する
- 翻訳の改善や対応言語の追加
- ドキュメントの充実

## Issueの作成
- バグレポートと翻訳の修正はテンプレートに従って作成してください。
- テンプレートの改善案も歓迎します。

## 対応言語の追加
このプロジェクトは多言語対応を目指しており、新しい言語の追加を歓迎します。新しい言語をプロジェクトに追加したい場合は、以下のプロセスに従ってください。
1. **言語提案の提出**: GitLabのIssueを通じて、新しい言語の追加を提案してください。
2. **翻訳プロセスの開始**: 提案が承認されたら、翻訳を開始してください。翻訳ファイル(`ftl`ファイル)はGitLabのマージリクエストを通じて提出してください。
3. **レビューとフィードバック**: 現在レビュアーがいないため、翻訳レビューはプロジェクトの管理者である私が行います。翻訳の精度を確認するため、`DeepL Translate`やその他の翻訳ツールを補助的に使用することがあります。
4. **翻訳のマージ**: レビューを経て問題がなければ、翻訳はmainブランチにマージされ、次のリリースで公開されます。