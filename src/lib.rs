/* Copyright 2024-2025 Neuron Grid

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. */

pub mod app_errors;
pub mod generate_pass;
pub mod i18n;
pub mod user_interface;

pub use app_errors::{GenerationError, Result};
pub use generate_pass::generate_password_flow;
pub use i18n::{initialize_bundle, parse_args, RupassArgs};
pub use user_interface::StdioInterface;
